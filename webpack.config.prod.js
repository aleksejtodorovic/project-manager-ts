const path = require('path');

const CleanPlugin = require('clean-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
    mode: 'production',
    entry: './src/app.ts',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    devtool: 'none',
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: 'ts-loader',
                exclude: /node_modules/
            }
        ]
    },
    resolve: {
        extensions: ['.ts', '.js']
    },
    plugins: [new CleanPlugin.CleanWebpackPlugin(), new CopyPlugin({
        patterns: [
            path.posix.join(
                path.resolve(__dirname).replace(/\\/g, '/'),
                '*.css'
            ),
            path.posix.join(
                path.resolve(__dirname).replace(/\\/g, '/'),
                '*.html'
            )
        ]
    })]
};
