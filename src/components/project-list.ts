import { Component } from './base-component';
import { Project } from '../models/project';
import { Autobind } from '../decorators/autobind';
import ProjectItem from './project-item';
import { DragTarget } from '../models/drag-and-drop';
import { ProjectState } from '../state/project-state';
import { ProjectStatus } from '../models/project';

export default class ProjectList
    extends Component<HTMLDivElement, HTMLElement>
    implements DragTarget {
    projects: Project[] = [];

    constructor(private type: ProjectStatus) {
        super('project-list', 'app', false, `${type}-projects`);
        this.element.className = 'projects';

        ProjectState.getInstance().addListener(this.projectStateChangeHandler);
        this.renderContent();
    }

    @Autobind
    private projectStateChangeHandler(projects: Project[]) {
        this.projects = projects.filter((project) => project.status === this.type);

        this.configure();
        this.renderProjects();
    }

    private renderProjects() {
        const listEl = document.getElementById(
            `${this.type}-projects-list`
        )! as HTMLUListElement;

        listEl.innerHTML = '';

        for (const project of this.projects) {
            new ProjectItem(this.element.querySelector('ul')!.id, project);
        }
    }

    @Autobind
    dragOverHandler(event: DragEvent) {
        if (event.dataTransfer && event.dataTransfer.types[0] === 'text/plain') {
            event.preventDefault(); // tell js browser that this element we want to allow drop, otherwise drop event wont fire
            const listEl = this.element.querySelector('ul')!;
            listEl.classList.add('droppable');
        }
    }

    @Autobind
    dragLeaveHandler(event: DragEvent) {
        const listEl = this.element.querySelector('ul')!;
        listEl.classList.remove('droppable');
    }

    @Autobind
    dropHandler(event: DragEvent) {
        const projId = event.dataTransfer!.getData('text/plain');

        ProjectState.getInstance().moveProject(projId, this.type);
    }

    protected configure() {
        this.element.addEventListener('dragover', this.dragOverHandler);
        this.element.addEventListener('dragleave', this.dragLeaveHandler);
        this.element.addEventListener('drop', this.dropHandler);
    }

    protected renderContent() {
        const listId = `${this.type}-projects-list`;
        this.element.querySelector('ul')!.id = listId;
        this.element.querySelector('h2')!.textContent =
            this.type.toUpperCase() + ' PROJECTS';
    }
}
